/*
 * Copyright 2011 The Emscripten Authors.  All rights reserved.
 * Emscripten is available under two separate licenses, the MIT license and the
 * University of Illinois/NCSA Open Source License.  Both these licenses can be
 * found in the LICENSE file.
 */

//  > emcc hello.c -o hello.js -s WASM=1 -s EXPORTED_FUNCTIONS='["_addOne","_malloc", "_free"]'
// > emcc  --bind -o hello.js hello.cpp -s NO_EXIT_RUNTIME=0
// . emcc -- bind - o hello.js hello.cpp -s NO_EXIT_RUNTIME = 0
// #include <stdio.h>
#include <iostream>
#include <time.h>
#include </home/noriyo/emsdk/upstream/emscripten/system/include/emscripten/emscripten.h>
#include </home/noriyo/emsdk/upstream/emscripten/system/include/emscripten/bind.h>
#include </home/noriyo/emsdk/upstream/emscripten/system/include/emscripten/val.h>

// #include <emscripten/bind.h>

using namespace emscripten;

class Shell
{
public:
    int eid;
    int pid;
    int n1;
    int n2;
    int n3;
    int n4;
    bool isDuplicate;
    // private:
    //     class shell *sh;
};

val Greeting(int time)
{
    return val("Hi emscripten::val");
}

val readJSObject(val obj)
{
    puts("readJSObject");
    printf("eid: %d", obj["eid"].as<int>());
    puts("");
    obj.set("isDuplicate", true);
    return obj;
}

void findDuplicateShell2(class Shell shell1, class Shell shell2)
{
    // console.log('findDuplicateShell');
    // console.log(shells[j]);
    if (shell1.n3 == shell1.n4)
    {
        if (shell2.n3 == shell2.n4)
        {
            if ((shell1.n1 == shell2.n1) && (shell1.n2 == shell2.n3) && (shell1.n3 == shell2.n2))
            {
                shell1.isDuplicate = true;
                shell2.isDuplicate = true;
            }
            if ((shell1.n1 == shell2.n3) && (shell1.n2 == shell2.n2) && (shell1.n3 == shell2.n1))
            {
                shell1.isDuplicate = true;
                shell2.isDuplicate = true;
            }
            if ((shell1.n1 == shell2.n2) && (shell1.n2 == shell2.n1) && (shell1.n3 == shell2.n3))
            {
                shell1.isDuplicate = true;
                shell2.isDuplicate = true;
            }
        }
    }
    else
    {
        if ((shell1.n1 == shell2.n4) && (shell1.n2 == shell2.n3) && (shell1.n3 == shell2.n2) && (shell1.n4 == shell2.n1))
        {
            shell1.isDuplicate = true;
            shell2.isDuplicate = true;
        }
        if ((shell1.n1 == shell2.n3) && (shell1.n2 == shell2.n2) && (shell1.n3 == shell2.n1) && (shell1.n4 == shell2.n4))
        {
            shell1.isDuplicate = true;
            shell2.isDuplicate = true;
        }
        if ((shell1.n1 == shell2.n2) && (shell1.n2 == shell2.n1) && (shell1.n3 == shell2.n4) && (shell1.n4 == shell2.n3))
        {
            shell1.isDuplicate = true;
            shell2.isDuplicate = true;
        }
        if ((shell1.n1 == shell2.n1) && (shell1.n2 == shell2.n4) && (shell1.n3 == shell2.n3) && (shell1.n4 == shell2.n2))
        {
            shell1.isDuplicate = true;
            shell2.isDuplicate = true;
        }
    }
    // puts("aaa");
    return;
}

void findDuplicateShell(val shell1, val shell2)
{
    if (shell1["n3"].as<int>() == shell1["n4"].as<int>())
    {
        // puts("shell1:tria");
        if (shell2["n3"].as<int>() == shell2["n4"].as<int>())
        {
            // puts("shell2:tria");
            if ((shell1["n1"].as<int>() == shell2["n1"].as<int>()) &&
                (shell1["n2"].as<int>() == shell2["n3"].as<int>()) &&
                (shell1["n3"].as<int>() == shell2["n2"].as<int>()))
            {
                shell1.set("isDuplicate", true);
                shell2.set("isDuplicate", true);
                return;
            }
            if ((shell1["n1"].as<int>() == shell2["n3"].as<int>()) &&
                (shell1["n2"].as<int>() == shell2["n2"].as<int>()) &&
                (shell1["n3"].as<int>() == shell2["n1"].as<int>()))
            {
                shell1.set("isDuplicate", true);
                shell2.set("isDuplicate", true);
                return;
            }
            if ((shell1["n1"].as<int>() == shell2["n2"].as<int>()) &&
                (shell1["n2"].as<int>() == shell2["n1"].as<int>()) &&
                (shell1["n3"].as<int>() == shell2["n3"].as<int>()))
            {
                shell1.set("isDuplicate", true);
                shell2.set("isDuplicate", true);
                return;
            }
        }
        else
        {
            return;
        }
    }
    else
    {
        // puts("else");
        if ((shell1["n1"].as<int>() == shell2["n4"].as<int>()) &&
            (shell1["n2"].as<int>() == shell2["n3"].as<int>()) &&
            (shell1["n3"].as<int>() == shell2["n2"].as<int>()) &&
            (shell1["n4"].as<int>() == shell2["n1"].as<int>()))
        {
            // puts("1234 = 4321");
            shell1.set("isDuplicate", true);
            shell2.set("isDuplicate", true);
            return;
        }
        if ((shell1["n1"].as<int>() == shell2["n3"].as<int>()) &&
            (shell1["n2"].as<int>() == shell2["n2"].as<int>()) &&
            (shell1["n3"].as<int>() == shell2["n1"].as<int>()) &&
            (shell1["n3"].as<int>() == shell2["n4"].as<int>()))
        {
            // puts("1234 = 3214");
            shell1.set("isDuplicate", true);
            shell2.set("isDuplicate", true);
            return;
        }
        if ((shell1["n1"].as<int>() == shell2["n2"].as<int>()) &&
            (shell1["n2"].as<int>() == shell2["n1"].as<int>()) &&
            (shell1["n3"].as<int>() == shell2["n4"].as<int>()) &&
            (shell1["n4"].as<int>() == shell2["n3"].as<int>()))
        {
            // puts("1234 = 2143");
            shell1.set("isDuplicate", true);
            shell2.set("isDuplicate", true);
            return;
        }
        if ((shell1["n1"].as<int>() == shell2["n1"].as<int>()) &&
            (shell1["n2"].as<int>() == shell2["n4"].as<int>()) &&
            (shell1["n3"].as<int>() == shell2["n3"].as<int>()) &&
            (shell1["n4"].as<int>() == shell2["n2"].as<int>()))
        {
            // puts("1234 = 1432");
            shell1.set("isDuplicate", true);
            shell2.set("isDuplicate", true);
            return;
        }
    }
    return;
}

void setDuplicateShellFlag(val shell1, val shell2, int length)
{
    int i, j;
    i = 0;
    j = 0;
    while (i < length)
    {
        j = length - 1;
        while (j > i)
        {
            findDuplicateShell(shell1, shell2);
            j = (j - 1);
        }
        i = (i + 1);
    }
}

// class Shell dot(class Shell sh1, class Shell sh2)
val dot(class Shell sh1, class Shell sh2)
{
    // Shell sh;
    // sh1.eid = 101;
    // sh1.isDuplicate = true;
    printf("s1: %d\t", sh1.isDuplicate);
    printf("s2: %d\n", sh2.isDuplicate);
    if (sh1.isDuplicate)
    {
        puts("sh1: true\n");
    }
    else
    {
        puts("sh1: false\n");
    }
    if (sh2.isDuplicate)
    {
        puts("sh2: true\n");
    }
    else
    {
        puts("sh2: false\n");
    }
    sh1.eid = 10022;
    sh1.isDuplicate = true;

    // declear array for return
    val ret = val::array();
    ret.call<val>("push", val(sh1));
    ret.call<val>("push", val(sh2));
    return ret;
}

// val dot2(val sh1)
// {
//     printf("sh1: %d\t", sh1.eid);
//     return val(sh1);
// }

int dot2(class Shell *sh)
{
    // Shell sh;
    // sh1.eid = 101;
    return sh[0].eid + sh[1].eid;
}

int LoopTest(val shell1, val shell2, int length)
{
    int n1, n2, n3, n4;
    int i, j;
    int ret;
    i = 0;
    j = 0;
    while (i < length)
    {
        j = length - 1;
        while (j > i)
        {
            n1 = shell1["n1"].as<int>();
            n2 = shell1["n2"].as<int>();
            n3 = shell1["n3"].as<int>();
            n4 = shell1["n4"].as<int>();
            ret = i * 10;
            j = (j - 1);
        }
        i = (i + 1);
    }

    return ret;
}

int main()
{
    Shell sh1, sh2;

    sh1.eid = 9100;
    sh1.pid = 10;
    sh1.n1 = 1;
    sh1.n2 = 2;
    sh1.n3 = 3;
    sh1.n4 = 4;
    sh1.isDuplicate = false;
    sh2.eid = 9100;
    sh2.pid = 10;
    sh2.n1 = 1;
    sh2.n2 = 4;
    sh2.n3 = 3;
    sh2.n4 = 2;
    sh2.isDuplicate = false;

    clock_t start = clock();

    int length = 5000;
    int i = 0;
    int j = 0;
    while (i < length)
    {
        j = length - 1;
        while (j > i)
        {
            findDuplicateShell2(sh1, sh2);
            j = (j - 1);
        }
        i = (i + 1);
    }

    clock_t end = clock();

    const double time = static_cast<double>(end - start) / CLOCKS_PER_SEC * 1000.0;
    printf("time %lf[ms]\n", time);
    printf("%d", sh1.isDuplicate);

    return 0;
}

EMSCRIPTEN_BINDINGS(myModule)
{
    // value_array<Shell2>("Shell2")
    //     .element(&Shell2::eid)
    //     .element(&Shell2::pid);

    class_<Shell>("Shell")
        .constructor<>()
        // .constructor<int, int>()
        // .function("norm", &Shell::dot)
        // .function("dot", &Shell::dot3)
        .property("eid", &Shell::eid)
        .property("pid", &Shell::pid)
        .property("n1", &Shell::n1)
        .property("n2", &Shell::n2)
        .property("n3", &Shell::n3)
        .property("n4", &Shell::n4)
        .property("isDuplicate", &Shell::isDuplicate);
    function("dot", &dot);
    // function("dot2", &dot2);
    function("Greeting", &Greeting);
    function("readJSObject", &readJSObject);
    function("findDuplicateShell", &findDuplicateShell);
    function("findDuplicateShell2", &findDuplicateShell2);
    function("setDuplicateShellFlag", &setDuplicateShellFlag);
    function("LoopTest", &LoopTest);
};